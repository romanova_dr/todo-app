export interface ICategory {
  id: string;
  name: string;
  title: string;
  date: number;
  removable: boolean;
  uid?: string;
}

export interface ITask {
  id: string;
  title: string;
  text: string;
  done: boolean;
  category: string;
  date: number;
  uid?: string;
}

export type THeaderContext = {
  alert: {
    show: boolean;
    type?: 'success' | 'info' | 'warning' | 'error';
    text?: string;
  };
  setAlert: (alert: THeaderContext['alert']) => void;
  edit: boolean;
  setEdit: (value: boolean) => void;
  searchValue: string;
  setSearchValue: (value: string) => void;
  title: string;
  setTitle: (value: string) => void;
};
