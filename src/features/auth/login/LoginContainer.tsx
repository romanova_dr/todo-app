import React, { FC, Reducer, useReducer, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { LoginForm, TLoginField } from '@components/LoginForm/LoginForm';
import { routes } from '../../../config/routes';
import { TLoginWithEmailAndPasswordResult } from '../types';
import { validateEmail } from './utils';
import { useAuthContext } from '../AuthContextProvider';

type TLoginFormFieldState = Omit<TLoginField, 'onChange'>;

type Action = { type: 'change' | 'error'; value: string };

function reducer(state: TLoginFormFieldState, action: Action): TLoginFormFieldState {
  switch (action.type) {
    case 'change':
      return {
        ...state,
        error: false,
        helper: '',
        value: action.value,
      };
    case 'error':
      return {
        ...state,
        error: true,
        helper: action.value,
      };
    default:
      throw new Error();
  }
}

export const LoginContainer: FC = () => {
  const history = useHistory();
  const { state: locationState } = useLocation<{ from: string }>();
  const { loginWithEmailAndPassword, loginWithOauthPopup } = useAuthContext();
  const [authError, setAuthError] = useState('');
  const [emailState, dispatchEmail] = useReducer<Reducer<TLoginFormFieldState, Action>>(reducer, {
    name: 'email',
    value: '',
  });
  const [passwordState, dispatchPassword] = useReducer<Reducer<TLoginFormFieldState, Action>>(reducer, {
    name: 'password',
    value: '',
  });

  const processLogin = (loginPromise: Promise<TLoginWithEmailAndPasswordResult>) => {
    return loginPromise
      .then(() => {
        history.push(locationState?.from || routes.home);
      })
      .catch((error) => {
        setAuthError(error?.message || 'error');
      });
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let valid = true;
    if (!validateEmail(emailState.value)) {
      dispatchEmail({
        type: 'error',
        value: 'Email is not correct',
      });
      valid = false;
    }

    if (passwordState.value.length <= 6) {
      dispatchPassword({
        type: 'error',
        value: 'Password must be more then 6 symbols',
      });
      valid = false;
    }

    if (valid) {
      processLogin(loginWithEmailAndPassword(emailState.value, passwordState.value));
    }
  };

  const onOauthLogin = (e: React.MouseEvent<HTMLElement>, provider: string) => {
    e.preventDefault();
    if (provider) {
      processLogin(loginWithOauthPopup(provider));
    }
  };

  return (
    <LoginForm
      email={{
        ...emailState,
        onChange: (e) => dispatchEmail({ type: 'change', value: e.target.value }),
      }}
      password={{
        ...passwordState,
        onChange: (e) => dispatchPassword({ type: 'change', value: e.target.value }),
      }}
      onSubmit={onSubmit}
      onOauthLogin={onOauthLogin}
      authError={authError}
    />
  );
};
