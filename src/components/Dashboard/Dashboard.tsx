import React, { FC, useEffect, useState } from 'react';
import { Box, CircularProgress, Grid } from '@mui/material';
import { CategoryItem } from '@components/Dashboard/CategoryItem/CategoryItem';
import { useAuthContext } from '../../features/auth/AuthContextProvider';
import { useHeaderContext } from '@components/Header/HeaderProvider';
import { colors } from '../../config/colors';
import { deleteCategory, getAllCategories } from '../../config/api';
import { ICategory } from '../../types';

export const Dashboard: FC = () => {
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [searchList, setSearchList] = useState<ICategory[]>([]);
  const [loading, setLoading] = useState(false);

  const { edit, searchValue, setTitle, setAlert } = useHeaderContext();
  const { user } = useAuthContext();

  useEffect(() => {
    setTitle('');
    fetchCategories();
  }, []);

  useEffect(() => {
    if (!searchValue) {
      setSearchList([]);
      return;
    }
    setSearchList(categories?.filter((elem) => elem?.title?.toLowerCase()?.includes(searchValue)));
  }, [searchValue]);

  const dataList = () => {
    if (searchValue) return searchList;
    return categories;
  };

  const fetchCategories = () => {
    setLoading(true);
    getAllCategories(user)
      .then((res) => {
        setCategories(res);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const deleteCategoryById = (id: string) => {
    deleteCategory(id, user)
      .then(() => {
        setAlert({ show: true, type: 'success', text: `Category and all category's tasks were successfully deleted!` });
        setCategories(categories?.filter((elem) => elem?.id !== id));
        if (searchValue) {
          setSearchList(searchList?.filter((elem) => elem?.id !== id));
        }
      })
      .catch((err) => {
        setAlert({ show: true, type: 'error', text: err?.message });
      });
  };

  const renderCards = () => {
    if (searchValue && !dataList()?.length) {
      return (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 5 }}>
          <p>Данных не найдено...</p>
        </Box>
      );
    }

    return (
      <>
        {dataList()?.map((elem) => (
          <Grid item xs={3} key={elem?.id}>
            <CategoryItem card={elem} fetchCategories={fetchCategories} deleteCategory={deleteCategoryById} />
          </Grid>
        ))}

        {!searchValue && !edit && (
          <Grid item xs={3}>
            <CategoryItem card={null} fetchCategories={fetchCategories} />
          </Grid>
        )}
      </>
    );
  };

  return (
    <>
      {loading ? (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
          <CircularProgress sx={{ color: colors.primary, mt: 5 }} />
        </Box>
      ) : (
        <Grid container spacing={4} sx={{ mb: 5 }}>
          {renderCards()}
        </Grid>
      )}
    </>
  );
};
