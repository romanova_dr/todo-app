import * as React from 'react';
import { ChangeEvent, FC, FormEvent } from 'react';
import { Box, CircularProgress, Dialog as MUIDialog, DialogTitle, Stack } from '@mui/material';
import { CustomTextField } from '@components/Custom/textfield';
import { SubmitBtn } from '@components/Custom/submit.btn';
import { styled } from '@mui/material/styles';
import { colors } from '../../../config/colors';

const Dialog = styled(MUIDialog)(() => ({
  '& .MuiPaper-root': {
    boxShadow: 'none',
    borderRadius: 50,
    padding: '30px',
  },
}));

export type TField = {
  error?: boolean;
  helper?: string;
  value: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
};

type TProps = {
  field: TField;
  loading: boolean;
  open: boolean;
  handleClose: (value?: string) => void;
  onSubmit: (event: FormEvent<HTMLFormElement>) => void;
};

export const AddCategory: FC<TProps> = ({ open, handleClose, field, loading, onSubmit }) => {
  return (
    <Dialog onClose={() => handleClose()} open={open} fullWidth={true} maxWidth="xs">
      <DialogTitle sx={{ fontFamily: 'Montserrat Alternates, sans-serif', fontSize: '25px', color: colors.dark }}>
        Add new category
      </DialogTitle>

      {loading && (
        <CircularProgress sx={{ color: colors.primary, alignSelf: 'center', position: 'absolute', top: '40%' }} />
      )}

      <Box sx={{ m: 4, position: 'relative', ...(loading && { opacity: '0.4' }) }}>
        <form onSubmit={onSubmit}>
          <Stack direction="column" spacing={3} justifyContent="center" alignItems="center">
            <CustomTextField
              fullWidth
              label="Title"
              name="title"
              value={field.value}
              onChange={field.onChange}
              error={!!field.error}
              helperText={field.helper}
              disabled={loading}
              variant="outlined"
            />
            <SubmitBtn type="submit" disabled={loading}>
              Save
            </SubmitBtn>
          </Stack>
        </form>
      </Box>
    </Dialog>
  );
};
