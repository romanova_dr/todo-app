import * as React from 'react';
import { FC, FormEvent, Reducer, useReducer, useState } from 'react';
import { Link } from 'react-router-dom';
import { Box, Card as MUICard, CardContent, IconButton, IconProps, SxProps, Tooltip } from '@mui/material';
import { styled } from '@mui/material/styles';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import CategoryIcon from '@mui/icons-material/Category';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import EventIcon from '@mui/icons-material/Event';
import GroupsIcon from '@mui/icons-material/Groups';
import ListIcon from '@mui/icons-material/List';
import ModeOfTravelIcon from '@mui/icons-material/ModeOfTravel';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import WorkIcon from '@mui/icons-material/Work';
import { AddCategory, TField } from '@components/Dashboard/AddCategory/AddCategory';
import { useAuthContext } from '../../../features/auth/AuthContextProvider';
import { useHeaderContext } from '@components/Header/HeaderProvider';
import { baseCategories } from '../../../config/utils';
import { colors } from '../../../config/colors';
import { createCategory, updateCategory } from '../../../config/api';
import { ICategory } from '../../../types';
import './CategoryItem.css';

const Card = styled(MUICard)(() => ({
  minHeight: '200px',
  minWidth: '200px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  border: 'none',
  backgroundColor: colors.white,
  borderRadius: 50,
  boxShadow: 'rgba(0, 0, 0, 4%) 0px 3px 5px',
  transition: '300ms',
  '&:hover': {
    cursor: 'pointer',
    transform: 'scale(1.02)',
  },
  '& .MuiCardContent-root': {
    padding: '8px',
  },
  '& .MuiCardContent-root:last-child': {
    padding: '8px',
  },
}));

const icon = (id: string, style: SxProps): IconProps => {
  switch (id) {
    case baseCategories.all:
      return <CategoryIcon sx={style} />;
    case baseCategories.events:
      return <EventIcon sx={style} />;
    case baseCategories.meeting:
      return <GroupsIcon sx={style} />;
    case baseCategories.personal:
      return <PersonOutlineIcon sx={style} />;
    case baseCategories.shopping:
      return <ShoppingCartIcon sx={style} />;
    case baseCategories.work:
      return <WorkIcon sx={style} />;
    case baseCategories.travel:
      return <ModeOfTravelIcon sx={style} />;
    default:
      return <ListIcon sx={style} />;
  }
};

type TFormFieldState = Omit<TField, 'onChange'>;

type Action = { type: 'change' | 'error'; value: string };

function reducer(state: TFormFieldState, action: Action): TFormFieldState {
  switch (action.type) {
    case 'change':
      return {
        ...state,
        error: false,
        helper: '',
        value: action.value,
      };
    case 'error':
      return {
        ...state,
        error: true,
        helper: action.value,
      };
    default:
      throw new Error();
  }
}

type TProps = {
  card: ICategory | null;
  fetchCategories: () => void;
  deleteCategory?: (id: string) => void;
};

export const CategoryItem: FC<TProps> = ({ card, fetchCategories, deleteCategory }) => {
  const [loading, setLoading] = useState(false);
  const [showAddCategoryDialog, setShowAddCategoryDialog] = useState(false);
  const [category, dispatchCategory] = useReducer<Reducer<TFormFieldState, Action>>(reducer, { value: '' });

  const { edit } = useHeaderContext();
  const { user } = useAuthContext();

  const showDialog = (value = '') => {
    setShowAddCategoryDialog(!showAddCategoryDialog);
    dispatchCategory({
      type: 'change',
      value,
    });
  };

  const validate = (value: string) => {
    if (!value?.length) {
      dispatchCategory({
        type: 'error',
        value: 'Input category title',
      });
      return false;
    }

    if (value?.length > 30) {
      dispatchCategory({
        type: 'error',
        value: 'Max length is 30 symbols',
      });
      return false;
    }

    return true;
  };

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let valid = validate(category?.value);

    if (valid) {
      setLoading(true);
      createCategory(category.value, user)
        .then(() => {
          fetchCategories();
          showDialog();
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
          dispatchCategory({
            type: 'error',
            value: 'Unable to save category',
          });
          valid = false;
        });
    }
  };

  const onEdit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let valid = validate(category?.value);

    if (valid) {
      setLoading(true);
      updateCategory(
        card?.id ?? '',
        {
          title: category?.value,
          date: card?.date ?? Date.now(),
          removable: card?.removable ?? true,
        },
        user
      )
        .then(() => {
          fetchCategories();
          showDialog();
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
          dispatchCategory({
            type: 'error',
            value: 'Unable to update category',
          });
          valid = false;
        });
    }
  };

  if (card === null) {
    return (
      <>
        <Card sx={{ opacity: '0.4' }} onClick={() => showDialog()}>
          <CardContent>
            <AddCircleOutlineIcon sx={{ color: colors.primary, fontSize: 50 }} />
          </CardContent>
        </Card>
        <AddCategory
          open={showAddCategoryDialog}
          handleClose={showDialog}
          onSubmit={onSubmit}
          field={{
            ...category,
            onChange: (e) => dispatchCategory({ type: 'change', value: e.target.value }),
          }}
          loading={loading}
        />
      </>
    );
  }

  const renderCard = () => {
    return (
      <Card>
        <CardContent sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <>
            {icon(card?.id ?? '', { color: colors.primary, fontSize: 40, mb: 2 })}
            <p className="category-item__title">{card?.title}</p>{' '}
          </>
        </CardContent>
      </Card>
    );
  };

  return (
    <>
      {!edit ? (
        <Link
          to={{
            pathname: `/${card?.name}`,
            state: { categoryId: card?.id },
          }}
        >
          {renderCard()}
        </Link>
      ) : (
        renderCard()
      )}

      {edit && card?.removable && (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', gap: 1, mt: 1 }}>
          <IconButton onClick={() => showDialog(card?.title)}>
            <EditIcon sx={{ color: colors.primary }} />
          </IconButton>

          <Tooltip title="All tasks of this category will be deleted">
            <IconButton
              onClick={() => {
                if (deleteCategory) {
                  deleteCategory(card?.id);
                }
              }}
            >
              <DeleteIcon sx={{ color: colors.primary }} />
            </IconButton>
          </Tooltip>
        </Box>
      )}

      <AddCategory
        open={showAddCategoryDialog}
        handleClose={showDialog}
        onSubmit={onEdit}
        field={{
          ...category,
          onChange: (e) => dispatchCategory({ type: 'change', value: e.target.value }),
        }}
        loading={loading}
      />
    </>
  );
};
