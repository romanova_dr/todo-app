import React, { FC } from 'react';
import { Box, CircularProgress } from '@mui/material';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { routes } from '../../config/routes';
import { useAuthContext } from '../../features/auth/AuthContextProvider';
import { colors } from '../../config/colors';

type TProps = {
  children: React.ReactNode;
} & RouteProps;

export const PrivateRoute: FC<TProps> = ({ children, ...rest }) => {
  const { isAuthenticated } = useAuthContext();

  if (isAuthenticated === null) {
    return (
      <Box sx={{ p: 4, textAlign: 'center' }}>
        <CircularProgress sx={{ color: colors.primary }} />
      </Box>
    );
  }
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: routes.login,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
