import React, { FC } from 'react';
import { Header } from '@components/Header/Header';
import { HeaderContextProvider } from '@components/Header/HeaderProvider';
import './Page.css';

type TProps = {
  children: React.ReactNode;
};

export const Page: FC<TProps> = ({ children }) => {
  return (
    <HeaderContextProvider>
      <section className="page-container">
        <Header />
        <main>{children}</main>
      </section>
    </HeaderContextProvider>
  );
};
