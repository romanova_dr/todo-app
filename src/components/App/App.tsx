import React, { FC, useEffect } from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import { Dashboard } from '@components/Dashboard/Dashboard';
import { LoginContainer } from '../../features/auth/login/LoginContainer';
import { Page } from '@components/Page/Page';
import { PrivateRoute } from '@components/PrivateRoute/PrivateRoute';
import { Tasks } from '@components/Tasks/Tasks';
import { routes } from '../../config/routes';

export const App: FC = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <Switch>
      <Route path={routes.login}>
        <LoginContainer />
      </Route>
      <PrivateRoute exact path={routes.home}>
        <Page>
          <Dashboard />
        </Page>
      </PrivateRoute>
      <PrivateRoute exact path={routes.category}>
        <Page>
          <Tasks />
        </Page>
      </PrivateRoute>
    </Switch>
  );
};
