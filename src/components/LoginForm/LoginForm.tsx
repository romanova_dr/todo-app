import React, { FC } from 'react';
import { ProviderId } from 'firebase/auth';
import { Box, IconButton, Stack } from '@mui/material';
import AddTaskIcon from '@mui/icons-material/AddTask';
import GoogleIcon from '@mui/icons-material/Google';
import GitHubIcon from '@mui/icons-material/GitHub';
import LoginIcon from '@mui/icons-material/Login';
import { CustomTextField } from '@components/Custom/textfield';
import { SubmitBtn } from '@components/Custom/submit.btn';
import { ALLOWED_OAUTH_PROVIDERS } from '../../features/auth/AuthContextProvider';
import { colors } from '../../config/colors';
import './LoginForm.css';

const getOAuthProviderIcon = (provider: string) => {
  switch (provider) {
    case ProviderId.GOOGLE:
      return <GoogleIcon sx={{ color: colors.primary, fontSize: '36px' }} />;
    case ProviderId.GITHUB:
      return <GitHubIcon sx={{ color: colors.primary, fontSize: '36px' }} />;
    default:
      return <LoginIcon sx={{ color: colors.primary, fontSize: '36px' }} />;
  }
};

export type TLoginField = {
  name: string;
  error?: boolean;
  helper?: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

type TProps = {
  authError: string;
  email: TLoginField;
  password: TLoginField;
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  onOauthLogin: (e: React.MouseEvent<HTMLElement>, provider: string) => void;
};

export const LoginForm: FC<TProps> = ({ authError, email, password, onSubmit, onOauthLogin }) => {
  return (
    <Box className="container login-form-container">
      <form onSubmit={onSubmit} className="login-form">
        <Stack direction="column" spacing={3} justifyContent="center" alignItems="center">
          <AddTaskIcon className="login-form__icon" sx={{ color: '#518099', fontSize: 70 }} />
          <p className="login-form__title">Welcome</p>
          {authError && <p className="login-form__error">{authError}</p>}
          <CustomTextField
            fullWidth
            label={email?.name}
            name={email?.name}
            value={email?.value}
            onChange={email?.onChange}
            error={!!email?.error}
            helperText={email?.helper}
            variant="outlined"
          />
          <CustomTextField
            fullWidth
            type="password"
            label={password?.name}
            name={password?.name}
            value={password?.value}
            onChange={password?.onChange}
            error={!!password?.error}
            helperText={password?.helper}
            variant="outlined"
          />
          <SubmitBtn type="submit">Log in</SubmitBtn>
          <div className="login-form__oauth-container">
            {Object.keys(ALLOWED_OAUTH_PROVIDERS).map((item) => {
              return (
                <IconButton key={item} onClick={(e) => onOauthLogin(e, item)}>
                  {getOAuthProviderIcon(item)}
                </IconButton>
              );
            })}
          </div>
        </Stack>
      </form>
    </Box>
  );
};
