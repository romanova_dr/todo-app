import React, { FC } from 'react';
import { Alert as MUIAlert, AlertProps } from '@mui/material';
import { styled } from '@mui/material/styles';
import { colors } from '../../config/colors';

const Alert = styled(MUIAlert)(() => ({
  '&.MuiAlert-root': {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '30px',
    borderRadius: '50px',
    color: colors.dark,
    fontSize: '17px',
    fontFamily: 'Montserrat Alternates, sans-serif',
    '.MuiAlert-action': {
      color: colors.dark,
    },
  },
  '&.MuiAlert-outlinedSuccess': {
    border: '1px solid #4caf50',
    '.MuiAlert-icon': {
      color: '#4caf50',
    },
  },
  '&.MuiAlert-outlinedInfo': {
    border: `1px solid ${colors.primary}`,
    '.MuiAlert-icon': {
      color: colors.primary,
    },
  },
  '&.MuiAlert-outlinedWarning': {
    border: `1px solid ${colors.accentLight}`,
    '.MuiAlert-icon': {
      color: colors.accentLight,
    },
  },
  '&.MuiAlert-outlinedError': {
    border: `1px solid ${colors.accentLight}`,
    '.MuiAlert-icon': {
      color: colors.accentLight,
    },
  },
}));

export const CustomAlert: FC<AlertProps> = ({ ...props }) => {
  return <Alert variant="outlined" {...props} />;
};
