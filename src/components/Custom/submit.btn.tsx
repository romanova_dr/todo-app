import React, { FC } from 'react';
import { Button as MUIButton } from '@mui/material';
import { ButtonProps } from '@mui/material/Button/Button';
import { styled } from '@mui/material/styles';
import { colors } from '../../config/colors';

const Button = styled(MUIButton)(() => ({
  fontFamily: 'Montserrat Alternates, sans-serif',
  textTransform: 'none',
  backgroundColor: colors.primary,
  borderRadius: '20px',
  width: '150px',
  '&:hover': {
    backgroundColor: colors.primaryHover,
  },
}));

export const SubmitBtn: FC<ButtonProps> = ({ ...props }) => {
  return <Button variant="contained" size="large" {...props} />;
};
