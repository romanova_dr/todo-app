import React, { FC } from 'react';
import { TextField as MUITextField, TextFieldProps } from '@mui/material';
import { styled } from '@mui/material/styles';
import { colors } from '../../config/colors';

const TextField = styled(MUITextField)(() => ({
  '& label': {
    fontFamily: 'Montserrat Alternates, sans-serif',
  },
  '& label.Mui-focused': {
    color: colors.primary,
  },
  '& .MuiInputBase-root': {
    borderRadius: '50px',
    '&.Mui-focused fieldset': {
      borderColor: colors.primary,
    },
    '&:hover fieldset': {
      borderColor: colors.primary,
    },
  },
  '& .MuiInputBase-input': {
    color: colors.dark,
    fontFamily: 'Montserrat Alternates, sans-serif',
  },
}));

export const CustomTextField: FC<TextFieldProps> = ({ ...props }) => {
  return <TextField {...props} />;
};
