import React, { createContext, FC, useContext, useState } from 'react';
import { THeaderContext } from '../../types';

export const headerContext = createContext<THeaderContext>({
  alert: {
    show: false,
    type: 'info',
    text: '',
  },
  setAlert: () => void 0,
  edit: false,
  setEdit: () => void 0,
  searchValue: '',
  setSearchValue: () => void 0,
  title: '',
  setTitle: () => void 0,
});

export const useHeaderContext = (): THeaderContext => {
  return useContext<THeaderContext>(headerContext);
};

type TProps = {
  children: React.ReactNode;
};

export const HeaderContextProvider: FC<TProps> = (props) => {
  const [edit, setEdit] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [title, setTitle] = useState('');
  const [alert, setAlert] = useState<THeaderContext['alert']>({ show: false, type: 'info', text: '' });

  return (
    <headerContext.Provider
      value={{
        alert,
        setAlert,
        edit,
        setEdit,
        searchValue,
        setSearchValue,
        title,
        setTitle,
      }}
    >
      {props.children}
    </headerContext.Provider>
  );
};
