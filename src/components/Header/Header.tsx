import * as React from 'react';
import { FC } from 'react';
import { Avatar, Box as MUIBox, IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import EditOffIcon from '@mui/icons-material/EditOff';
import LogoutIcon from '@mui/icons-material/Logout';
import { styled } from '@mui/material/styles';
import { CustomAlert } from '@components/Custom/alert';
import { CustomTextField } from '@components/Custom/textfield';
import { Link, useHistory } from 'react-router-dom';
import { useAuthContext } from '../../features/auth/AuthContextProvider';
import { useHeaderContext } from '@components/Header/HeaderProvider';
import { colors } from '../../config/colors';
import { routes } from '../../config/routes';
import './Header.css';

const Box = styled(MUIBox)(() => ({
  width: '100%',
  maxWidth: '100%',
  minHeight: '90px',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  margin: '30px 0',
  backgroundColor: colors.light,
  borderRadius: 50,
  boxShadow: 'rgba(0, 0, 0, 4%) 0px 3px 5px',
}));

export const Header: FC = () => {
  const history = useHistory();
  const { user, isAuthenticated, logOut } = useAuthContext();
  const { alert, setAlert, edit, setEdit, searchValue, setSearchValue, title } = useHeaderContext();

  const onLogOut = () => {
    logOut();
    history.push(routes.home);
  };

  return (
    <>
      <Box>
        <p className="header__title">
          {edit ? (
            <p className="header__link">{'Todo'}</p>
          ) : (
            <Link to={routes.home} className="header__link">
              {'Todo'}
            </Link>
          )}
          {title && <p>{`/ ${title}`}</p>}
        </p>

        {isAuthenticated && (
          <MUIBox
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <CustomTextField
              label="Search"
              name="search"
              value={searchValue}
              onChange={(event) => setSearchValue(event.target.value)}
              sx={{ mr: '20px', width: '175px' }}
              variant="outlined"
            />
            <IconButton sx={{ mr: '20px' }} onClick={() => setEdit(!edit)}>
              {edit ? (
                <EditOffIcon sx={{ fontSize: '30px', color: colors.primaryDark }} />
              ) : (
                <EditIcon sx={{ fontSize: '30px', color: colors.primaryDark }} />
              )}
            </IconButton>
            <Avatar
              alt="avatar"
              src={user?.photoURL || 'https://picsum.photos/200/300'}
              sx={{ width: 50, height: 50 }}
            />
            <IconButton sx={{ ml: '20px', mr: '30px' }} onClick={onLogOut}>
              <LogoutIcon sx={{ fontSize: '30px', color: colors.primaryDark }} />
            </IconButton>
          </MUIBox>
        )}
      </Box>
      {alert?.show && (
        <CustomAlert
          severity={alert?.type}
          onClose={() => {
            setAlert({ show: false });
          }}
        >
          {alert?.text}
        </CustomAlert>
      )}
    </>
  );
};
