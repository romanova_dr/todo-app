import * as React from 'react';
import { useLocation } from 'react-router-dom';
import { FC, FormEvent, Reducer, useEffect, useReducer, useState } from 'react';
import { Box, Card as MUICard, CardContent, Checkbox, IconButton } from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { styled } from '@mui/material/styles';
import { AddTask, TField } from '@components/Tasks/AddTask/AddTask';
import { getErrors, InputErrors, InputName } from '@components/Tasks/TaskItem/helper';
import { useAuthContext } from '../../../features/auth/AuthContextProvider';
import { useHeaderContext } from '@components/Header/HeaderProvider';
import { baseCategories } from '../../../config/utils';
import { colors } from '../../../config/colors';
import { createTask, updateTask } from '../../../config/api';
import { ICategory, ITask } from '../../../types';
import './TaskItem.css';

const Card = styled(MUICard)(() => ({
  minHeight: '100px',
  minWidth: '250px',
  maxWidth: '450px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  border: 'none',
  backgroundColor: colors.white,
  borderRadius: 30,
  boxShadow: 'rgba(0, 0, 0, 4%) 0px 3px 5px',
  transition: '300ms',
  '&:hover': {
    cursor: 'pointer',
  },
  '& .MuiCardContent-root': {
    padding: '8px',
  },
  '& .MuiCardContent-root:last-child': {
    padding: '8px',
  },
}));

type TFormFieldState = {
  title: Omit<TField, 'onChange'>;
  text: Omit<TField, 'onChange'>;
  category: Omit<TField, 'onChange'>;
};

type Action = { type: 'change' | 'error' | 'reset'; name: InputName; value: string };

function reducer(state: TFormFieldState, action: Action): TFormFieldState {
  switch (action.type) {
    case 'reset':
      return {
        ...state,
        title: { value: '' },
        text: { value: '' },
        category: { value: '' },
      };
    case 'change':
      return {
        ...state,
        [action?.name]: {
          ...state[action?.name],
          error: false,
          helper: '',
          value: action.value,
        },
      };
    case 'error':
      return {
        ...state,
        [action?.name]: {
          ...state[action?.name],
          error: true,
          helper: action.value,
        },
      };
    default:
      throw new Error();
  }
}

type TProps = {
  task: ITask | null;
  categories: ICategory[];
  fetchTasks: () => void;
  deleteTask?: (id: string, done?: boolean) => void;
};

export const TaskItem: FC<TProps> = ({ task, fetchTasks, deleteTask, categories }) => {
  const { edit } = useHeaderContext();
  const { user } = useAuthContext();
  const { state }: { state: { categoryId: string } } = useLocation();

  const [taskCategory, setTaskCategory] = useState<ICategory | null>(null);
  const [loading, setLoading] = useState(false);
  const [showAddTaskDialog, setShowAddTaskDialog] = useState(false);
  const [options, setOptions] = useState<ICategory[]>([]);
  const [checkbox, setCheckbox] = useState(false);
  const [formData, dispatchFormData] = useReducer<Reducer<TFormFieldState, Action>>(reducer, {
    title: { value: '' },
    text: { value: '' },
    category: { value: '' },
  });

  useEffect(() => {
    setTaskCategory(categories?.find((elem) => elem?.id === task?.category) ?? null);
    setCheckbox(task?.done ?? false);

    const current = categories?.find((elem) => elem?.id === state?.categoryId) ?? null;

    if (current?.id === baseCategories.all) {
      setOptions(categories);
    } else {
      setOptions(current ? [current] : []);
    }
  }, []);

  const showDialog = () => {
    setShowAddTaskDialog(!showAddTaskDialog);
    dispatchFormData({
      type: 'reset',
      value: '',
      name: '' as InputName,
    });
  };

  const onEdit = () => {
    showDialog();
    if (task) {
      Object.entries(task)?.forEach(([name, value]) => {
        if (name === 'title' || name === 'text' || name === 'category') {
          dispatchFormData({
            type: 'change',
            value,
            name: name as InputName,
          });
        }
      });
    }
  };

  const fetchErrors = async () => {
    const fields = new FormData();
    Object.entries(formData)?.forEach(([name, data]) => {
      fields.append(name, data?.value);
    });

    const errors: InputErrors = await getErrors(Array.from(fields.entries()) as [InputName, FormDataEntryValue][]);
    const hasErrors = Object.values(errors)?.some((elem) => elem);

    if (hasErrors) {
      Object.entries(errors)?.forEach(([name, value]) => {
        if (value) {
          dispatchFormData({
            type: 'error',
            value,
            name: name as InputName,
          });
        }
      });
    }

    return hasErrors;
  };

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const isNotValid = await fetchErrors();
    if (!isNotValid) {
      setLoading(true);
      createTask(
        {
          title: formData?.title?.value,
          text: formData?.text?.value,
          category: formData?.category?.value,
        },
        user
      )
        .then(() => {
          fetchTasks();
          showDialog();
          setLoading(false);
        })
        .catch(() => {
          showDialog();
          setLoading(false);
        });
    }
  };

  const onUpdate = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const isNotValid = await fetchErrors();
    if (!isNotValid) {
      setLoading(true);
      updateTask(
        task?.id ?? '',
        {
          ...task,
          title: formData?.title?.value,
          text: formData?.text?.value,
          category: formData?.category?.value,
          date: task?.date ?? Date.now(),
          done: task?.done ?? false,
        },
        user
      )
        .then(() => {
          fetchTasks();
          showDialog();
          setLoading(false);
        })
        .catch(() => {
          showDialog();
          setLoading(false);
        });
    }
  };

  if (task === null) {
    return (
      <>
        <Card sx={{ opacity: '0.4', minHeight: '200px' }} onClick={() => showDialog()}>
          <CardContent>
            <AddCircleOutlineIcon sx={{ color: colors.primary, fontSize: 50 }} />
          </CardContent>
        </Card>

        <AddTask
          open={showAddTaskDialog}
          handleClose={showDialog}
          onSubmit={onSubmit}
          fields={{
            ...formData,
            onChange: (value: string, name: InputName) => dispatchFormData({ type: 'change', value, name }),
          }}
          loading={loading}
          options={options}
        />
      </>
    );
  }

  return (
    <>
      <Card
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
          p: '10px',
        }}
      >
        <CardContent>
          <div className="task-item__label">{taskCategory?.title}</div>
        </CardContent>

        <CardContent>
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <p className="task-item__title">{task?.title}</p>
            <Checkbox
              sx={{
                color: colors.primary,
                '&.Mui-checked': {
                  color: colors.primary,
                },
              }}
              checked={checkbox}
              onChange={() => {
                setCheckbox(!checkbox);
                setTimeout(() => {
                  if (deleteTask) {
                    deleteTask(task?.id, true);
                  }
                }, 1000);
              }}
            />
          </Box>
        </CardContent>

        <CardContent>
          <p className="task-item__text">{task?.text}</p>
        </CardContent>
      </Card>

      {edit && (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', gap: 1, mt: 1 }}>
          <IconButton onClick={() => onEdit()}>
            <EditIcon sx={{ color: colors.primary }} />
          </IconButton>
          <IconButton
            onClick={() => {
              if (deleteTask) {
                deleteTask(task?.id);
              }
            }}
          >
            <DeleteIcon sx={{ color: colors.primary }} />
          </IconButton>
        </Box>
      )}

      <AddTask
        open={showAddTaskDialog}
        handleClose={showDialog}
        onSubmit={onUpdate}
        fields={{
          ...formData,
          onChange: (value: string, name: InputName) => dispatchFormData({ type: 'change', value, name }),
        }}
        loading={loading}
        options={options}
      />
    </>
  );
};
