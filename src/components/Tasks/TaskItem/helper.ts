export type InputName = 'title' | 'text' | 'category';

export type InputErrors = {
  [key in InputName]: string;
};

export const getErrors = async (data: [InputName, FormDataEntryValue][]): Promise<InputErrors> => {
  const errors: InputErrors = {
    title: '',
    text: '',
    category: '',
  };

  for (const [name, value] of data) {
    if (typeof value !== 'string') {
      break;
    }

    switch (name) {
      case 'title':
        if (!value?.length) {
          errors[name] = 'Input task title';
          break;
        }
        if (value?.length > 30) {
          errors[name] = 'Max length is 30 symbols';
          break;
        }
        break;

      case 'text':
        if (value?.length > 140) {
          errors[name] = 'Max length is 140 symbols';
          break;
        }
        break;

      case 'category':
        if (!value?.length) {
          errors[name] = 'Input task category';
          break;
        }
        break;

      default:
        break;
    }
  }

  return errors;
};
