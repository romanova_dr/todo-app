import * as React from 'react';
import { FC, FormEvent } from 'react';
import { Box, CircularProgress, Dialog as MUIDialog, DialogTitle, Stack } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import { CustomTextField } from '@components/Custom/textfield';
import { SubmitBtn } from '@components/Custom/submit.btn';
import { styled } from '@mui/material/styles';
import { colors } from '../../../config/colors';
import { ICategory } from '../../../types';
import { InputName } from '@components/Tasks/TaskItem/helper';

const Dialog = styled(MUIDialog)(() => ({
  '& .MuiPaper-root': {
    boxShadow: 'none',
    borderRadius: 50,
    padding: '30px',
  },
}));

export type TField = {
  error?: boolean;
  helper?: string;
  value: string;
  onChange: (value: string, name: InputName) => void;
};

type TProps = {
  fields: {
    title: Omit<TField, 'onChange'>;
    text: Omit<TField, 'onChange'>;
    category: Omit<TField, 'onChange'>;
    onChange: TField['onChange'];
  };
  loading: boolean;
  open: boolean;
  options: ICategory[];
  handleClose: (value?: string) => void;
  onSubmit: (event: FormEvent<HTMLFormElement>) => void;
};

export const AddTask: FC<TProps> = ({ open, handleClose, loading, onSubmit, options, fields }) => {
  return (
    <Dialog onClose={() => handleClose()} open={open} fullWidth={true} maxWidth="xs">
      <DialogTitle sx={{ fontFamily: 'Montserrat Alternates, sans-serif', fontSize: '25px', color: colors.dark }}>
        Add new task
      </DialogTitle>

      {loading && (
        <CircularProgress sx={{ color: colors.primary, alignSelf: 'center', position: 'absolute', top: '40%' }} />
      )}

      <Box sx={{ m: 4, position: 'relative', ...(loading && { opacity: '0.4' }) }}>
        <form onSubmit={onSubmit}>
          <Stack direction="column" spacing={3} justifyContent="center" alignItems="center">
            <CustomTextField
              fullWidth
              select
              label="Category"
              name="category"
              value={fields?.category?.value}
              onChange={(e) => fields?.onChange(e?.target?.value, 'category')}
              error={!!fields?.category?.error}
              helperText={fields?.category?.helper}
              disabled={loading}
              variant="outlined"
            >
              {options.map((option) => (
                <MenuItem key={option.id} value={option.id} sx={{ fontFamily: 'Montserrat Alternates, sans-serif' }}>
                  {option.title}
                </MenuItem>
              ))}
            </CustomTextField>

            <CustomTextField
              fullWidth
              label="Title"
              name="title"
              value={fields?.title?.value}
              onChange={(e) => fields?.onChange(e?.target?.value, 'title')}
              error={!!fields?.title?.error}
              helperText={fields?.title?.helper}
              disabled={loading}
              variant="outlined"
            />
            <CustomTextField
              fullWidth
              multiline
              rows={3}
              label="Description"
              name="text"
              value={fields?.text?.value}
              onChange={(e) => fields?.onChange(e?.target?.value, 'text')}
              error={!!fields?.text?.error}
              helperText={fields?.text?.helper}
              disabled={loading}
              variant="outlined"
            />
            <SubmitBtn type="submit" disabled={loading}>
              Save
            </SubmitBtn>
          </Stack>
        </form>
      </Box>
    </Dialog>
  );
};
