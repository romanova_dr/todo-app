import React, { FC, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Box, CircularProgress, Grid } from '@mui/material';
import { TaskItem } from '@components/Tasks/TaskItem/TaskItem';
import { useAuthContext } from '../../features/auth/AuthContextProvider';
import { useHeaderContext } from '@components/Header/HeaderProvider';
import { baseCategories } from '../../config/utils';
import { colors } from '../../config/colors';
import { getAllTasks, getTasksByCategory, deleteTask, getAllCategories } from '../../config/api';
import { ICategory, ITask } from '../../types';

export const Tasks: FC = () => {
  const [loading, setLoading] = useState(false);
  const [tasks, setTasks] = useState<ITask[]>([]);
  const [searchList, setSearchList] = useState<ITask[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);

  const { user } = useAuthContext();
  const { edit, searchValue, setTitle, setAlert } = useHeaderContext();
  const { state }: { state: { categoryId: string } } = useLocation();

  useEffect(() => {
    getAllCategories(user).then((res) => {
      setCategories(res);
      setTitle(res?.find((elem) => elem?.id === state?.categoryId)?.title ?? '');
    });
    fetchTasks();
  }, []);

  useEffect(() => {
    if (!searchValue) {
      setSearchList([]);
      return;
    }
    setSearchList(tasks?.filter((elem) => elem?.title?.toLowerCase()?.includes(searchValue)));
  }, [searchValue]);

  const fetchTasks = () => {
    setLoading(true);

    if (state?.categoryId === baseCategories.all) {
      getAllTasks(user)
        .then((res) => {
          setTasks(res);
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        });
    } else {
      getTasksByCategory(state?.categoryId, user)
        .then((res) => {
          setTasks(res);
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        });
    }
  };

  const dataList = () => {
    if (searchValue) return searchList;
    return tasks;
  };

  const deleteTaskById = (id: string, done = false) => {
    deleteTask(id)
      .then(() => {
        setAlert({
          show: true,
          type: 'success',
          text: done ? 'Good job! Task was successfully done!' : 'Task was successfully deleted!',
        });

        setTasks(tasks?.filter((elem) => elem?.id !== id));

        if (searchValue) {
          setSearchList(searchList?.filter((elem) => elem?.id !== id));
        }
      })
      .catch((err) => {
        setAlert({ show: true, type: 'error', text: err?.message });
      });
  };

  const renderCards = () => {
    if (searchValue && !dataList()?.length) {
      return (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 5 }}>
          <p>Данных не найдено...</p>
        </Box>
      );
    }

    return (
      <>
        {dataList().map((elem) => (
          <Grid item xs={4} key={elem?.id}>
            <TaskItem task={elem} categories={categories} fetchTasks={fetchTasks} deleteTask={deleteTaskById} />
          </Grid>
        ))}

        {!searchValue && !edit && (
          <Grid item xs={4}>
            <TaskItem task={null} categories={categories} fetchTasks={fetchTasks} />
          </Grid>
        )}
      </>
    );
  };

  return (
    <>
      {loading ? (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
          <CircularProgress sx={{ color: colors.primary, mt: 5 }} />
        </Box>
      ) : (
        <Grid container spacing={4} sx={{ mb: 5 }}>
          {renderCards()}
        </Grid>
      )}
    </>
  );
};
