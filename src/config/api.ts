import { initializeApp, FirebaseApp } from 'firebase/app';
import { getAuth, User } from 'firebase/auth';
import { ICategory, ITask } from '../types';
import { collection, getDocs, getFirestore, addDoc, query, doc, deleteDoc, where, updateDoc } from 'firebase/firestore';
import { sortByDate } from './utils';

export let firebaseApp: FirebaseApp;

export const initializeAPI = (): FirebaseApp => {
  firebaseApp = initializeApp({
    apiKey: 'AIzaSyC84IXNaAzeCJpUdmslXyKT5Gy7GP8x16M',
    authDomain: 'todo-list-app-254db.firebaseapp.com',
    projectId: 'todo-list-app-254db',
    storageBucket: 'todo-list-app-254db.appspot.com',
    messagingSenderId: '561847943006',
    appId: '1:561847943006:web:b5688481e601d0b56d59cf',
    measurementId: 'G-08JNQ5THPV',
  });

  getAuth(firebaseApp);
  getFirestore(firebaseApp);

  return firebaseApp;
};

/** === Categories ===*/
const categoriesCollection = 'categories';

export const getAllCategories = async (user: User | null): Promise<ICategory[]> => {
  const db = getFirestore();
  let categories: ICategory[] = [];

  try {
    const select = query(collection(db, categoriesCollection), where('uid', 'in', ['', user?.uid]));
    const querySnapshot = await getDocs(select);

    querySnapshot.forEach((doc) => {
      const data = doc.data() as Omit<ICategory, 'id'>;
      categories = sortByDate([...categories, { id: doc.id, ...data }]) as ICategory[];
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return categories;
};

export const createCategory = async (title: string, user: User | null): Promise<any> => {
  const db = getFirestore();
  try {
    await addDoc(collection(db, categoriesCollection), {
      title,
      name: title?.toLowerCase()?.split(' ')?.join('_'),
      removable: true,
      date: Date.now(),
      uid: user?.uid,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteCategory = async (id: string, user: User | null): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, categoriesCollection, id);

  try {
    await deleteDoc(ref);
    const tasks = await getTasksByCategory(id, user);
    for (const task of tasks) {
      await deleteTask(task?.id);
    }
  } catch (error) {
    return Promise.reject(error);
  }
};

export const updateCategory = async (
  id: string,
  data: Omit<ICategory, 'id' | 'name'>,
  user: User | null
): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, categoriesCollection, id);

  try {
    await updateDoc(ref, { ...data, name: data?.title?.toLowerCase()?.split(' ')?.join('_'), uid: user?.uid });
  } catch (error) {
    return Promise.reject(error);
  }
};

/** === Tasks === */
const tasksCollection = 'tasks';

export const getAllTasks = async (user: User | null): Promise<ITask[]> => {
  const db = getFirestore();
  let tasks: ITask[] = [];

  try {
    const select = query(collection(db, tasksCollection), where('uid', '==', user?.uid));
    const querySnapshot = await getDocs(select);
    querySnapshot.forEach((doc) => {
      const data = doc.data() as Omit<ITask, 'id'>;
      tasks = sortByDate([...tasks, { id: doc.id, ...data }]) as ITask[];
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return tasks;
};

export const getTasksByCategory = async (categoryId: string, user: User | null): Promise<ITask[]> => {
  const db = getFirestore();
  let tasks: ITask[] = [];
  try {
    const q = query(
      collection(db, tasksCollection),
      where('category', '==', categoryId),
      where('uid', '==', user?.uid)
    );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      const data = doc.data() as Omit<ITask, 'id'>;
      tasks = sortByDate([...tasks, { id: doc.id, ...data }]) as ITask[];
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return tasks;
};

export const createTask = async (data: Omit<ITask, 'id' | 'date' | 'done'>, user: User | null): Promise<any> => {
  const db = getFirestore();
  try {
    await addDoc(collection(db, tasksCollection), {
      ...data,
      date: Date.now(),
      done: false,
      uid: user?.uid,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteTask = async (id: string): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, tasksCollection, id);

  try {
    await deleteDoc(ref);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const updateTask = async (id: string, data: Omit<ITask, 'id'>, user: User | null): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, tasksCollection, id);

  try {
    await updateDoc(ref, { ...data, uid: user?.uid });
  } catch (error) {
    return Promise.reject(error);
  }
};
