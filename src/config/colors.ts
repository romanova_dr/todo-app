export const colors: { [key: string]: string } = {
  white: '#fff',
  light: '#f0f5f9',
  primaryLight: '#b2cfde',
  secondaryLight: '#dce8ef',
  primary: '#518099',
  primaryHover: '#518099d4',
  primaryDark: '#4b6d7e',
  dark: '#1d313b',
  accentLight: '#ff844b',
  accent: '#ffe0d2',
};
