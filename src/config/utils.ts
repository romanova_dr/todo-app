import { ICategory, ITask } from '../types';

export enum baseCategories {
  all = 'bezfctD3GulveHm4eWS3',
  shopping = 'J3nvg5utEUXTmJ0a8lxL',
  travel = 'OwwPksWMj38I0afLiaIt',
  personal = 'UToJLRx5yKv0tP7cbXe2',
  events = 'WVgpXxmpHGMYwIfA2qcn',
  meeting = 'Zsi7sTwDE972TBLMFW6Y',
  work = 'gtFhD531Rtq833AwKLJZ',
}

export const sortByDate = (arr: ICategory[] | ITask[]): ICategory[] | ITask[] => {
  return arr?.sort((prev, next) => {
    if (prev?.date > next?.date) return 1;
    if (prev?.date < next?.date) return -1;
    return 0;
  });
};
