export enum routes {
  home = '/',
  category = '/:category',
  login = '/login',
}
